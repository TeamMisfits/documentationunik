## Fault Reporting API DOCS - UNIK

## BASE URL: http://10.233.217.137/unik/api 


### CREATE FAULT 
#### METHOD: POST
#### ENDPOINT: "/index?action=createFault"
#### BODY TYPE: Form Data

```java
// Example 

RequestBody body = new MultipartBody.Builder().setType(MultipartBody.FORM)
  .addFormDataPart("faultType", "1")
  .addFormDataPart("faultDescription[]", "1")
  .addFormDataPart("serviceNo", "0302261146")
  .addFormDataPart("orderType", "1301")
  .addFormDataPart("modemSerial", "apiTest")
  .addFormDataPart("comments", "apiTestComment")
  .build();

```

#### Response<onSuccess>

```java
{
  "data": {
      "service_order_no": "118909"
  },
  "message": "Fault created successfully",
  "status": "200",
  "code": "success"
}
```

#### Response<onError>
```java
{
   data: null,
   message: "Invalid service number provided",
   status: "error ",
   code: "400"
}
```


###  GET FAULT TYPES 
#### METHOD: GET
#### ENDPOINT: "/index?action=faultTypes"

#### Response<onSuccess>
```java
	[
	 ...
     {
       FAULTYPE_ID: "3",
       FAULTTYPE_CD: "DATA ( DI,VPN,IPLC)",
       DESCRIPTION: "",
       FAULT: "2",
       DESC_DETAILS: [
         [Object], [Object], [Object],
         [Object], [Object], [Object],
         [Object], [Object], [Object],
         [Object], [Object], [Object],
         [Object], [Object], [Object],
         [Object], [Object], [Object],
         [Object], [Object]
       ]
     },
     {
       FAULTYPE_ID: "4",
       FAULTTYPE_CD: "IOT",
       DESCRIPTION: "",
       FAULT: "2",
       DESC_DETAILS: [ [Object], [Object], [Object], [Object], [Object] ]
     },
     {        FAULTYPE_ID: "5",
       FAULTTYPE_CD: "ISDN",
       DESCRIPTION: "",
       FAULT: "2",
       DESC_DETAILS: [
         [Object], [Object],
         [Object], [Object],
         [Object], [Object],
         [Object], [Object]
       ]
     },
     {
       FAULTYPE_ID: "6",
       FAULTTYPE_CD: "Xpress Wifi",
       DESCRIPTION: "",
       FAULT: "2",
       DESC_DETAILS: [
         [Object], [Object], [Object],
         [Object], [Object], [Object],
         [Object], [Object], [Object],
         [Object], [Object], [Object],
         [Object], [Object], [Object],
         [Object], [Object], [Object],
         [Object], [Object], [Object],
         [Object], [Object], [Object]
       ]
     },
     {
       FAULTYPE_ID: "7",
       FAULTTYPE_CD: "Network Outage",
       DESCRIPTION: "",
       FAULT: "2",
       DESC_DETAILS: [
         [Object], [Object],
         [Object], [Object],
         [Object], [Object],
         [Object]
       ]
     }
   ]
```

#### Individual faulTypesIndividualDetails Body<onSuccess>
```java
{
      FAULTYPE_ID: "3",
      FAULTTYPE_CD: "DATA ( DI,VPN,IPLC)",
      DESCRIPTION: "",
      FAULT: "2",
      DESC_DETAILS: [
        {
          TYPE: "3",
          FAULT_DESCRIPTION: "IoT not Transmiting",
          FAULT_CODE: "20"
        },
        { TYPE: "3", FAULT_DESCRIPTION: "Fibre Outage", FAULT_CODE: "21" },
        { TYPE: "3", FAULT_DESCRIPTION: "Broken pole", FAULT_CODE: "22" },
        { TYPE: "3", FAULT_DESCRIPTION: "DNS", FAULT_CODE: "23" },
        {
          TYPE: "3",
          FAULT_DESCRIPTION: "Outlook Not Working",
          FAULT_CODE: "24"
        },
        {
          TYPE: "3",
          FAULT_DESCRIPTION: "Cannot Send Emails",
          FAULT_CODE: "25"
        },        {
          TYPE: "3",
          FAULT_DESCRIPTION: "Cannot Receive Emails",
          FAULT_CODE: "26"
        },
        { TYPE: "3", FAULT_DESCRIPTION: "Packet Drops", FAULT_CODE: "27" },
        {
          TYPE: "3",
          FAULT_DESCRIPTION: "Intermittent breaks",
          FAULT_CODE: "28"
        },
        {
          TYPE: "3",
          FAULT_DESCRIPTION: "SIM not working",
          FAULT_CODE: "29"
        },
        {
          TYPE: "3",
          FAULT_DESCRIPTION: "Signal not strong",
          FAULT_CODE: "30"
        },
        { TYPE: "3", FAULT_DESCRIPTION: "APN", FAULT_CODE: "31" },
        { TYPE: "3", FAULT_DESCRIPTION: "AP", FAULT_CODE: "32" },
        { TYPE: "3", FAULT_DESCRIPTION: "Link Is Down", FAULT_CODE: "13" },
        {
          TYPE: "3",
          FAULT_DESCRIPTION: "Link Instability",
          FAULT_CODE: "14"
        },
        { TYPE: "3", FAULT_DESCRIPTION: "Slowness", FAULT_CODE: "15" },
        {
          TYPE: "3",
          FAULT_DESCRIPTION: "Experiencing Intermittent Breaks",
          FAULT_CODE: "16"
        },
        {
          TYPE: "3",
          FAULT_DESCRIPTION: "Not Getting Required Bandwidth",
          FAULT_CODE: "17"
        },
        {
          TYPE: "3",
          FAULT_DESCRIPTION: "Extremely Slow",
          FAULT_CODE: "18"
        },
        { TYPE: "3", FAULT_DESCRIPTION: "Latency", FAULT_CODE: "19" }
      ]
    }
```




###  GET ALL FAULT DESCRIPTIONS BY FAULT TYPE 
#### METHOD: GET
#### ENDPOINT: "/index?action=faultDescription&faultType=5"

#### Response<onSuccess>
```java
{
  data: [
    {
      TYPE: "5",
      FAULT_DESCRIPTION: "Cannot Make Calls",
      FAULT_CODE: "38"
    },
    {
      TYPE: "5",
      FAULT_DESCRIPTION: "Cannot Recevie Calls",
      FAULT_CODE: "39"
    },
    {
      TYPE: "5",
      FAULT_DESCRIPTION: "Cannot Make and Receive Calls",
      FAULT_CODE: "40"
    },
    { TYPE: "5", FAULT_DESCRIPTION: "No dail Tone", FAULT_CODE: "41" },
    { TYPE: "5", FAULT_DESCRIPTION: "Busy Tone", FAULT_CODE: "42" },
    { TYPE: "5", FAULT_DESCRIPTION: "Noise Line", FAULT_CODE: "43" },
    {
      TYPE: "5",
      FAULT_DESCRIPTION: "Canot hear the Caller",
      FAULT_CODE: "44"
    },
    {
      TYPE: "5",
      FAULT_DESCRIPTION: "Error message received",
      FAULT_CODE: "45"
    }
  ],
  message: "Fault Descriptions retrieved successfully",
  status: "200",
  code: "success"
}
```


###  GET ORDER TYPES 
#### METHOD: GET
#### ENDPOINT: "/index?action=getOrdertype"


#### Response<onSuccess>

```java
{
  data: [
    {
      ORDERTYPEID: "1301",
      ORDERTYPECD: "FTTH Faults",
      MODBY: "ADMIN",
      MODON: "22-MAY-19",
      ORDERTYPEDESC: "FTTH Faults",
      FEE: "",
      DELETED: "0",
      ORDERTYPE: "1"
    },
    {
      ORDERTYPEID: "1320",
      ORDERTYPECD: "Enterprise Faults",
      MODBY: "ADMIN",
      MODON: "05-NOV-19",
      ORDERTYPEDESC: "Enterprise Faults",
      FEE: "",
      DELETED: "0",
      ORDERTYPE: "2"
    }
  ],
  message: "Ordertype retrieved successfully",
  status: "200",
  code: "success"
}
```
